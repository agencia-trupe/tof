<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Conteudo;
use App\Models\ConteudoCategoria;

class ConteudoController extends Controller
{
    public function index(Request $request)
    {
        $view = 'frontend.conteudo.index';
        $data = [
            'posts' => Conteudo::ordenados()->get()
        ];

        return $request->ajax()
            ? view($view, $data)->renderSections()['content']
            : view($view, $data);
    }

    public function categoria(ConteudoCategoria $categoria, Request $request)
    {
        if (! $categoria->exists) abort('404');

        $view = 'frontend.conteudo.categoria';
        $data = [
            'categorias' => ConteudoCategoria::ordenados()->get(),
            'categoria'  => $categoria,
            'posts'      => Conteudo::ordenados()->categoria($categoria->id)->get()
        ];

        return $request->ajax()
            ? view($view, $data)->renderSections()['content']
            : view($view, $data);
    }

    public function post(ConteudoCategoria $categoria, Conteudo $post, Request $request)
    {
        if (! $categoria->exists|| ! $post->exists || $post->categoria != $categoria) {
            abort('404');
        }

        $view = 'frontend.conteudo.post';
        $data = [
            'categorias' => ConteudoCategoria::ordenados()->get(),
            'categoria'  => $categoria,
            'post'       => $post,
            'anteriores' => Conteudo::ordenados()
                                ->where('data', '<', $post->dataDb)
                                ->orWhere(function($query) use ($post) {
                                    $query->where('data', $post->dataDb)
                                          ->where('id', '<', $post->id);
                                    })
                                ->limit(3)->get()
        ];

        return $request->ajax()
            ? view($view, $data)->renderSections()['content']
            : view($view, $data);
    }
}
