<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;

class ContatoController extends Controller
{
    public function index(Request $request)
    {
        $view = 'frontend.contato';
        $data = [
            'contato' => Contato::first()
        ];

        return $request->ajax()
            ? view($view, $data)->renderSections()['content']
            : view($view, $data);
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $contatoRecebido->create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
