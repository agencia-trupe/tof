<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Banner;
use App\Models\Servicos;
use App\Models\Trabalho;
use App\Models\Socio;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $routeName = \Route::currentRouteName();

        $banners   = Banner::get();
        $servicos  = Servicos::first();
        $trabalho  = Trabalho::first();
        $socios    = Socio::ordenados()->get();

        return view('frontend.home', compact([
            'routeName', 'banners', 'servicos', 'trabalho', 'socios'
        ]));
    }
}
