<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Servicos;

class ServicosController extends Controller
{
    public function index(Request $request)
    {
        $view = 'frontend.servicos';
        $data = [
            'servicos' => Servicos::first()
        ];

        return $request->ajax()
            ? view($view, $data)->renderSections()['content']
            : view($view, $data);
    }
}
