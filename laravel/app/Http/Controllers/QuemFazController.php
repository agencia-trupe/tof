<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Socio;

class QuemFazController extends Controller
{
    public function index(Request $request)
    {
        $view = 'frontend.quem-faz';
        $data = [
            'socios' => Socio::ordenados()->get()
        ];

        return $request->ajax()
            ? view($view, $data)->renderSections()['content']
            : view($view, $data);
    }
}
