<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Trabalho;

class TrabalhoController extends Controller
{
    public function index(Request $request)
    {
        $view = 'frontend.trabalho';
        $data = [
            'trabalho' => Trabalho::first()
        ];

        return $request->ajax()
            ? view($view, $data)->renderSections()['content']
            : view($view, $data);
    }
}
