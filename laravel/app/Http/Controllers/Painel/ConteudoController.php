<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConteudoRequest;
use App\Http\Controllers\Controller;

use App\Models\Conteudo;
use App\Models\ConteudoCategoria;

class ConteudoController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = ConteudoCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index()
    {
        $registros = Conteudo::ordenados()->get();

        return view('painel.conteudo.index', compact('registros'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.conteudo.create', compact('categorias'));
    }

    public function store(ConteudoRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Conteudo::upload_capa();

            Conteudo::create($input);
            return redirect()->route('painel.conteudo.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Conteudo $registro)
    {
        $categorias = $this->categorias;

        return view('painel.conteudo.edit', compact('registro', 'categorias'));
    }

    public function update(ConteudoRequest $request, Conteudo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Conteudo::upload_capa();

            $registro->update($input);
            return redirect()->route('painel.conteudo.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Conteudo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.conteudo.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
