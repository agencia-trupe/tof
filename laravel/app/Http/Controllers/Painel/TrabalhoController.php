<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TrabalhoRequest;
use App\Http\Controllers\Controller;

use App\Models\Trabalho;

class TrabalhoController extends Controller
{
    public function index()
    {
        $registro = Trabalho::first();

        return view('painel.trabalho.edit', compact('registro'));
    }

    public function update(TrabalhoRequest $request, Trabalho $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Trabalho::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.trabalho.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
