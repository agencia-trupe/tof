<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\QuemFazRequest;
use App\Http\Controllers\Controller;

use App\Models\Socio;

class QuemFazController extends Controller
{
    public function index()
    {
        $registros = Socio::ordenados()->get();

        return view('painel.quem-faz.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.quem-faz.create');
    }

    public function store(QuemFazRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Socio::upload_imagem();

            Socio::create($input);
            return redirect()->route('painel.quem-faz.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Socio $registro)
    {
        return view('painel.quem-faz.edit', compact('registro'));
    }

    public function update(QuemFazRequest $request, Socio $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Socio::upload_imagem();

            $registro->update($input);
            return redirect()->route('painel.quem-faz.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Socio $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.quem-faz.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
