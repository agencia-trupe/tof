<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConteudoCategoriaRequest;
use App\Http\Controllers\Controller;

use App\Models\ConteudoCategoria;

class ConteudoCategoriasController extends Controller
{
    public function index()
    {
        $categorias = ConteudoCategoria::ordenados()->get();

        return view('painel.conteudo.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.conteudo.categorias.create');
    }

    public function store(ConteudoCategoriaRequest $request)
    {
        try {

            $input = $request->all();

            ConteudoCategoria::create($input);
            return redirect()->route('painel.conteudo.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(ConteudoCategoria $categoria)
    {
        return view('painel.conteudo.categorias.edit', compact('categoria'));
    }

    public function update(ConteudoCategoriaRequest $request, ConteudoCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.conteudo.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(ConteudoCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.conteudo.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
