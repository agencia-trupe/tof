<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConteudoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'conteudo_categoria_id' => 'required',
            'data'                  => 'required',
            'tempo_de_leitura'      => 'required',
            'titulo'                => 'required',
            'chamada'               => 'required',
            'capa'                  => 'required|image',
            'texto'                 => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
