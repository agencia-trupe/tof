<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'apresentacao' => 'required',
            'liderando_em_terradois' => 'required',
            'transformacao_cultural' => 'required',
            'etica_responsabilidade_crise' => 'required',
            'producao_e_edicao_de_cultura' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
        ];
    }
}
