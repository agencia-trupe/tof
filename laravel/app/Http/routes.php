<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('servicos', 'HomeController@index')->name('servicos');
    Route::get('o-trabalho', 'HomeController@index')->name('o-trabalho');
    Route::get('quem-faz', 'HomeController@index')->name('quem-faz');
    Route::get('conteudo', 'ConteudoController@index')->name('conteudo');
    Route::get('conteudo/{categoria_slug}', 'ConteudoController@categoria')->name('conteudo.categoria');
    Route::get('conteudo/{categoria_slug}/{post_slug}', 'ConteudoController@post')->name('conteudo.post');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('conteudo/categorias', 'ConteudoCategoriasController');
		Route::resource('conteudo', 'ConteudoController');
		Route::resource('servicos', 'ServicosController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController', [
            'except' => ['create', 'store', 'destroy']
        ]);
		Route::resource('trabalho', 'TrabalhoController', ['only' => ['index', 'update']]);
		Route::resource('quem-faz', 'QuemFazController', [
            'except' => ['create', 'store', 'destroy']
        ]);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
