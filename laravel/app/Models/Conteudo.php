<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Carbon\Carbon;

class Conteudo extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'conteudo';

    protected $guarded = ['id'];

    public function categoria()
    {
        return $this->belongsTo('App\Models\ConteudoCategoria', 'conteudo_categoria_id');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('conteudo_categoria_id', $categoria_id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getDataDbAttribute()
    {
        return Carbon::createFromFormat('d/m/Y', $this->data)->format('Y-m-d');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 285,
                'height' => 170,
                'path'   => 'assets/img/conteudo/capa/thumb/'
            ],
            [
                'width'  => 800,
                'height' => null,
                'path'   => 'assets/img/conteudo/capa/'
            ]
        ]);
    }

}
