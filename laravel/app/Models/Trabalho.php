<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Trabalho extends Model
{
    protected $table = 'trabalho';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 2000,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/trabalho/'
        ]);
    }

}
