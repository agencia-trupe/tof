<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Servicos extends Model
{
    protected $table = 'servicos';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 600,
            'height' => 660,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 600,
            'height' => 660,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 600,
            'height' => 660,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 600,
            'height' => 660,
            'path'   => 'assets/img/servicos/'
        ]);
    }

}
