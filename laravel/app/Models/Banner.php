<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Banner extends Model
{
    protected $table = 'banners';

    protected $guarded = ['id'];


    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 2000,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/banners/'
        ]);
    }

}
