(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.navegacaoHome = function() {
        $('.nav-home').click(function(event) {
            if (! $('.home').length) return;

            event.preventDefault();

            var classToScroll = '.' + $(this).data('nav');

            scrollToClass(classToScroll);

            $('.nav-home').removeClass('active');
            $(this).addClass('active');

            var href = $(this).attr('href');
            if (window.history.pushState) history.pushState({}, '', href);
        });

        $(window).on("popstate", function(event) {
            if (! $('.home').length) return;

            if (/servicos$/.test(location.pathname)) {
                scrollToClass('.servicos');
                $('.nav-home').removeClass('active');
                $('header nav a[data-nav=servicos]').addClass('active');
            } else if (/o-trabalho$/.test(location.pathname)) {
                scrollToClass('.o-trabalho');
                $('.nav-home').removeClass('active');
                $('header nav a[data-nav=o-trabalho]').addClass('active');
            } else if (/quem-faz$/.test(location.pathname)) {
                scrollToClass('.quem-faz');
                $('.nav-home').removeClass('active');
                $('header nav a[data-nav=quem-faz]').addClass('active');
            } else {
                scrollToClass('.home');
                $('.nav-home').removeClass('active');
                $('header nav a[data-nav=home]').addClass('active');
            }
        });

        function scrollToClass(classToScroll) {
            var padding = $('#left').width() > 0 ? 50 : 0;

            if (classToScroll == '.servicos' && padding == 0) classToScroll = '.servicos.mobile';

            $('html, body').animate({
                scrollTop: $(classToScroll).offset().top - padding
            }, 500);
        }

        if (typeof CURRENTROUTE !== 'undefined') {
            scrollToClass('.' + CURRENTROUTE);
        }
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.slide',
            speed: 1000,
            pagerTemplate: '<a href=#>{{slideNum}}</a>'
        });
    };

    App.textosComScroll = function() {
        var scrollsArray = [];

        $('.texto-scroll').each(function(index) {
            scrollsArray.push(new IScroll('#' + $(this).attr('id'), {
                mouseWheel : true,
                scrollbars : true,
            }));
        });

        var scrollsRefresh = function() {
            for (var i = scrollsArray.length - 1; i >= 0; i--) {
                scrollsArray[i].refresh();
            }
        };

        if ($('#texto-estatico').length) {
            var myscrollEstatico = new IScroll('#texto-estatico', {
                mouseWheel : true,
                scrollbars : true,
            });
            setTimeout(function() {
                myscrollEstatico.refresh();
            }, 500);
        }

        $('body').on('click', '.box-com-texto', function(event) {
            event.preventDefault();
            scrollsRefresh();
        });

        $('body').on('click', '.box-com-texto', function(event) {
            event.preventDefault();

            if($(this).hasClass('active')) return;

            $('.box-com-texto').removeClass('active').find('.texto').fadeOut();
            $(this).addClass('active').find('.texto').fadeIn();
            scrollsRefresh();
        });
    };

    App.envioContatoEvent = function() {
        $('body').on('submit', '#form-contato', App.envioContato);
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.init = function() {
        this.navegacaoHome();
        this.bannersHome();
        this.textosComScroll();
        this.envioContatoEvent();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
