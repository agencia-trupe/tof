@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/trabalho/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_1', 'Título 1') !!}
            {!! Form::text('titulo_1', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1', 'Texto 1') !!}
            {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_2', 'Título 2') !!}
            {!! Form::text('titulo_2', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2', 'Texto 2') !!}
            {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_3', 'Título 3') !!}
            {!! Form::text('titulo_3', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3', 'Texto 3') !!}
            {!! Form::textarea('texto_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
