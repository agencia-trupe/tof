@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Trabalho</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.trabalho.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.trabalho.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
