@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Quem faz /</small> Adicionar Sócio</h2>
    </legend>

    {!! Form::open(['route' => 'painel.quem-faz.store', 'files' => true]) !!}

        @include('painel.quem-faz.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
