@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Quem faz /</small> Editar Sócio</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.quem-faz.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.quem-faz.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
