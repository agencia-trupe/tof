@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('apresentacao', 'Apresentação') !!}
    {!! Form::textarea('apresentacao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('liderando_em_terradois', 'Liderando em TerraDois') !!}
            {!! Form::textarea('liderando_em_terradois', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('transformacao_cultural', 'Transformação cultural') !!}
            {!! Form::textarea('transformacao_cultural', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('etica_responsabilidade_crise', 'Ética Responsabilidade Crise') !!}
            {!! Form::textarea('etica_responsabilidade_crise', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('producao_e_edicao_de_cultura', 'Produção e edição de cultura') !!}
            {!! Form::textarea('producao_e_edicao_de_cultura', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            <img src="{{ url('assets/img/servicos/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            <img src="{{ url('assets/img/servicos/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
            <img src="{{ url('assets/img/servicos/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4') !!}
            <img src="{{ url('assets/img/servicos/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
