@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo /</small> Adicionar Conteúdo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.conteudo.store', 'files' => true]) !!}

        @include('painel.conteudo.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
