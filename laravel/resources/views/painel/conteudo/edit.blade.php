@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo /</small> Editar Conteúdo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.conteudo.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.conteudo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
