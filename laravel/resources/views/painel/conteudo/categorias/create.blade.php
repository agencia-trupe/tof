@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.conteudo.categorias.store']) !!}

        @include('painel.conteudo.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
