@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route' => ['painel.conteudo.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

        @include('painel.conteudo.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
