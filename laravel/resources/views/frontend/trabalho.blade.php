<div class="o-trabalho trabalho box-com-scroll" style="background-image:url('{{ asset('assets/img/trabalho/'.$trabalho->imagem) }}')">
    @foreach(range(1,3) as $i)
    <a href="#" class="trabalho-col box-com-texto">
        <div class="titulo">
            <span>{{ $trabalho->{'titulo_'.$i} }}</span>
        </div>
        <div class="texto texto-scroll" id="trabalho-texto-{{ $i }}">
            <div class="texto-wrapper">
                <p>{!! $trabalho->{'texto_'.$i} !!}</p>
            </div>
        </div>
    </a>
    @endforeach
</div>
