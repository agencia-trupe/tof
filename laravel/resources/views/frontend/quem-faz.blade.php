<div class="quem-faz">
    @foreach($socios as $socio)
    <div class="socio">
        <img src="{{ asset('assets/img/quem-faz/'.$socio->imagem) }}" alt="">
        <div class="texto">
            <h2>{{ $socio->nome }}</h2>
            <p>{{ $socio->descricao }}</p>
        </div>
    </div>
    @endforeach
</div>
