<div class="servicos box-com-scroll">
    <div class="box-1 box-peq" style="background-image:url('{{ asset('assets/img/servicos/'.$servicos->imagem_1) }}')"></div>

    <div class="box-2 box-texto-grande">
        <div id="texto-estatico">
            <div class="texto-wrapper">
                {!! $servicos->apresentacao !!}
            </div>
        </div>
    </div>

    <div class="box-3 box-peq" style="background-image:url('{{ asset('assets/img/servicos/'.$servicos->imagem_2) }}')"></div>

    <a href="#" class="box-4 box-peq box-com-texto">
        <div class="titulo">
            <h3>Liderando em<br>TerraDois</h3>
            <span class="icone icone-1"></span>
        </div>
        <div class="texto texto-scroll" id="texto-1">
            <div class="texto-wrapper">
                {!! $servicos->liderando_em_terradois !!}
            </div>
        </div>
    </a>

    <a href="#" class="box-5 box-peq box-com-texto">
        <div class="titulo">
            <h3>Transformação<br>cultural</h3>
            <span class="icone icone-2"></span>
        </div>
        <div class="texto texto-scroll" id="texto-2">
            <div class="texto-wrapper">
                {!! $servicos->transformacao_cultural !!}
            </div>
        </div>
    </a>

    <div class="box-6 box-peq" style="background-image:url('{{ asset('assets/img/servicos/'.$servicos->imagem_3) }}')"></div>

    <div class="box-7 box-peq" style="background-image:url('{{ asset('assets/img/servicos/'.$servicos->imagem_4) }}')"></div>

    <a href="#" class="box-8 box-peq box-com-texto">
        <div class="titulo">
            <h3>Ética<br>Responsabilidade<br>Crise</h3>
            <span class="icone icone-3"></span>
        </div>
        <div class="texto texto-scroll" id="texto-3">
            <div class="texto-wrapper">
                {!! $servicos->etica_responsabilidade_crise !!}
            </div>
        </div>
    </a>

    <a href="#" class="box-9 box-peq box-com-texto">
        <div class="titulo">
            <h3>Produção e edição<br>de cultura</h3>
            <span class="icone icone-4"></span>
        </div>
        <div class="texto texto-scroll" id="texto-4">
            <div class="texto-wrapper">
                {!! $servicos->producao_e_edicao_de_cultura !!}
            </div>
        </div>
    </a>
</div>

<div class="servicos mobile">
    <div class="box-mobile-texto">
        {!! $servicos->apresentacao !!}
    </div>

    <div class="box-mobile-imagem">
        <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_1) }}" alt="">
        <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_2) }}" alt="">
    </div>

    <div class="box-mobile-servico">
        <span class="icone icone-1"></span>
        <h3>Liderando em<br>TerraDois</h3>
        {!! $servicos->liderando_em_terradois !!}
    </div>

    <div class="box-mobile-servico servico-borda">
        <span class="icone icone-2"></span>
        <h3>Transformação<br>cultural</h3>
        {!! $servicos->transformacao_cultural !!}
    </div>

    <div class="box-mobile-imagem">
        <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_3) }}" alt="">
        <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_4) }}" alt="">
    </div>

    <div class="box-mobile-servico">
        <span class="icone icone-3"></span>
        <h3>Ética<br>Responsabilidade<br>Crise</h3>
        {!! $servicos->etica_responsabilidade_crise !!}
    </div>

    <div class="box-mobile-servico">
        <span class="icone icone-4"></span>
        <h3>Produção e edição<br>de cultura</h3>
        {!! $servicos->producao_e_edicao_de_cultura !!}
    </div>
</div>
