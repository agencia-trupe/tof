    <header>
        <nav>
            <a href="{{ route('home') }}" data-nav="home" class="nav-home @if(Route::currentRouteName() === 'home') active @endif">
                ToF
            </a>
            <a href="{{ route('servicos') }}" data-nav="servicos" class="nav-home @if(Route::currentRouteName() === 'servicos') active @endif">
                serviços
            </a>
            <a href="{{ route('o-trabalho') }}" data-nav="o-trabalho" class="nav-home @if(Route::currentRouteName() === 'o-trabalho') active @endif">
                o trabalho
            </a>
            <span class="mobile-div"></span>
            <a href="{{ route('quem-faz') }}" data-nav="quem-faz" class="nav-home @if(Route::currentRouteName() === 'quem-faz') active @endif">
                quem faz
            </a>
            <a href="{{ route('conteudo') }}" @if(str_is('conteudo*', Route::currentRouteName())) class="active" @endif>
                conteúdo
            </a>
            <a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif>
                contato
            </a>
        </nav>
    </header>
