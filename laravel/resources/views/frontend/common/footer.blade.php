    <footer>
        <div class="center">
            <p>© {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.</p>
        </div>
    </footer>
