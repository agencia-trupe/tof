@extends('frontend.common.template')

@section('content')

    <div class="conteudo">
        <div class="conteudo-categoria">
            <div class="categorias">
                @foreach($categorias as $cat)
                <a href="{{ route('conteudo.categoria', $cat->slug) }}" class="pjax-handle @if($cat->id === $categoria->id) active @endif">
                    {{ $cat->titulo }}
                </a>
                @endforeach
            </div>

            <div class="conteudo-index">
                @foreach($posts as $post)
                <a href="{{ route('conteudo.post', [$post->categoria->slug, $post->slug]) }}" class="conteudo-thumb pjax-handle">
                    <img src="{{ asset('assets/img/conteudo/capa/thumb/'.$post->capa) }}" alt="">
                    <span class="categoria">{{ $post->categoria->titulo }}</span>
                    <h2>{{ $post->titulo }}</h2>
                    <p>{{ $post->chamada }}</p>

                    <div class="informacoes">
                        <span class="data">{{ $post->data }}</span>
                        <span class="leitura">
                            TEMPO DE LEITURA: {{ $post->tempo_de_leitura }}
                        </span>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
