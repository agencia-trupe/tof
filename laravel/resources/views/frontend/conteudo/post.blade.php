@extends('frontend.common.template')

@section('content')

    <div class="conteudo">
        <div class="conteudo-post">
            <div class="categorias">
                @foreach($categorias as $cat)
                <a href="{{ route('conteudo.categoria', $cat->slug) }}" class="pjax-handle @if($cat->id === $categoria->id) active @endif">
                    {{ $cat->titulo }}
                </a>
                @endforeach
            </div>

            <div class="post">
                <span class="categoria">{{ $post->categoria->titulo }}</span>
                <div class="imagem">
                    <img src="{{ asset('assets/img/conteudo/capa/'.$post->capa) }}" alt="">
                    <div class="informacoes">
                        <span class="data">{{ $post->data }}</span>
                        <span class="leitura">
                            TEMPO DE LEITURA: {{ $post->tempo_de_leitura }}
                        </span>
                    </div>
                </div>
                <h1>{{ $post->titulo }}</h1>
                <div class="post-texto">{!! $post->texto !!}</div>
            </div>

            <div class="conteudo-index">
                @foreach($anteriores as $post)
                <a href="{{ route('conteudo.post', [$post->categoria->slug, $post->slug]) }}" class="conteudo-thumb pjax-handle">
                    <img src="{{ asset('assets/img/conteudo/capa/thumb/'.$post->capa) }}" alt="">
                    <span class="categoria">{{ $post->categoria->titulo }}</span>
                    <h2>{{ $post->titulo }}</h2>
                    <p>{{ $post->chamada }}</p>

                    <div class="informacoes">
                        <span class="data">{{ $post->data }}</span>
                        <span class="leitura">
                            TEMPO DE LEITURA: {{ $post->tempo_de_leitura }}
                        </span>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
