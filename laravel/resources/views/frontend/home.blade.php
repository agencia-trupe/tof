@extends('frontend.common.template')

@section('content')

    <div class="home-wrapper">
        <div class="home banners">
            @foreach($banners as $banner)
            <div class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                <div class="texto banner-{{ $banner->id }}">
                    @if($banner->id != 3) <div class="logo-tof">ToF</div> @endif
                    <p>{!! $banner->texto !!}</p>
                </div>
            </div>
            @endforeach
            <div class="cycle-pager"></div>
        </div>

        @include('frontend.servicos')

        @include('frontend.trabalho')

        @include('frontend.quem-faz')
    </div>

    <script>var CURRENTROUTE = '{{ $routeName }}';</script>

@endsection
