<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicosTable extends Migration
{
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('apresentacao');
            $table->text('liderando_em_terradois');
            $table->text('transformacao_cultural');
            $table->text('etica_responsabilidade_crise');
            $table->text('producao_e_edicao_de_cultura');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('servicos');
    }
}
