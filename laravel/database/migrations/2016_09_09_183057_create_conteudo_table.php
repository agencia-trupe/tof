<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConteudoTable extends Migration
{
    public function up()
    {
        Schema::create('conteudo_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('conteudo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('conteudo_categoria_id')->unsigned()->nullable();
            $table->string('data');
            $table->string('titulo');
            $table->string('slug');
            $table->text('chamada');
            $table->string('tempo_de_leitura');
            $table->string('capa');
            $table->text('texto');
            $table->timestamps();
            $table->foreign('conteudo_categoria_id')->references('id')->on('conteudo_categorias')->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::drop('conteudo');
        Schema::drop('conteudo_categorias');
    }
}
