<?php

use Illuminate\Database\Seeder;

class ServicosSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'apresentacao' => '',
            'liderando_em_terradois' => '',
            'transformacao_cultural' => '',
            'etica_responsabilidade_crise' => '',
            'producao_e_edicao_de_cultura' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
        ]);
    }
}
