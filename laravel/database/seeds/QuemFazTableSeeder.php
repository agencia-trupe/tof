<?php

use Illuminate\Database\Seeder;

class QuemFazTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quem_faz')->insert([
            ['nome' => 'Jorge Forbes', 'ordem' => 0],
            ['nome' => 'Silvio Genesini', 'ordem' => 1],
            ['nome' => 'Augusto Rodrigues', 'ordem' => 2],
            ['nome' => 'Ricardo Tucci', 'ordem' => 3],
        ]);
    }
}
