-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: tof
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'architect-graphics-abstract-building_20160916132436.jpg','A ToF &eacute; um laborat&oacute;rio da p&oacute;s-modernidade',NULL,'2016-09-16 13:56:44'),(2,'pexels-photo_20160916132524.jpg','Somos uma combina&ccedil;&atilde;o singular e exclusiva de especialistas em psican&aacute;lise, transforma&ccedil;&atilde;o cultural, inova&ccedil;&atilde;o e desenvolvimento de neg&oacute;cios',NULL,'2016-09-16 13:25:24'),(3,'blue-abstract-glass-balls_20160916132629.jpg','A <strong>ToF</strong> est&aacute; <strong>T</strong>raduzindo <strong>o</strong> <strong>F</strong>uturo',NULL,'2016-09-16 13:26:29');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@trupe.net','11 3456-7890','Rua do Endere&ccedil;o Completo, 345<br />\r\nBairro da Vila<br />\r\n01234-567 S&atilde;o Paulo SP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.3872932557697!2d-46.66623248451639!3d-23.55452988468625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59c8da0aa315%3A0xd59f9431f2c9776a!2sAv.+Paulista+-+Bela+Vista%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1474034331140\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>',NULL,'2016-09-16 13:58:56');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conteudo`
--

DROP TABLE IF EXISTS `conteudo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conteudo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conteudo_categoria_id` int(10) unsigned DEFAULT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada` text COLLATE utf8_unicode_ci NOT NULL,
  `tempo_de_leitura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `conteudo_conteudo_categoria_id_foreign` (`conteudo_categoria_id`),
  CONSTRAINT `conteudo_conteudo_categoria_id_foreign` FOREIGN KEY (`conteudo_categoria_id`) REFERENCES `conteudo_categorias` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conteudo`
--

LOCK TABLES `conteudo` WRITE;
/*!40000 ALTER TABLE `conteudo` DISABLE KEYS */;
INSERT INTO `conteudo` VALUES (1,1,'2016-09-16','Lorem ipsum dolor sit','lorem-ipsum-dolor-sit','Vestibulum eu libero fermentum, pulvinar libero in, hendrerit ex','5 min','pexels-photo-24578_20160916141320.jpg','<p>Mauris vitae risus et elit sagittis aliquam nec non libero. Vivamus fringilla condimentum diam, vel mollis orci tristique sed. Nulla nec urna et eros egestas luctus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>\r\n\r\n<p><a href=\"http://www.trupe.net\" target=\"_blank\">Exemplo de link</a></p>\r\n\r\n<p>Phasellus pellentesque cursus turpis, eu volutpat justo egestas a. Fusce sed congue magna. Donec tortor arcu, tincidunt eget maximus vitae, dapibus ut nibh. Ut sit amet velit in quam placerat placerat vitae ac mi. Phasellus pulvinar enim a purus ornare, quis pharetra augue lobortis. Sed non tristique tellus. Duis mollis, velit vel mollis tristique, erat tellus sollicitudin libero, non mollis dolor nisi eget dolor. Aliquam erat volutpat. Nunc hendrerit turpis ante, id elementum nibh finibus ac.</p>\r\n\r\n<p><img src=\"http://tof.dev/assets/img/conteudo/imagens/pexels-photo-38132_20160916141300.jpeg\" /></p>\r\n\r\n<p>Nam vel sollicitudin diam. Suspendisse potenti. Nulla dignissim magna ultrices condimentum tempus. Integer rhoncus vel ligula in laoreet. Proin eleifend eros in velit viverra sollicitudin. Quisque pulvinar molestie semper. Cras semper posuere interdum. Quisque dignissim feugiat dolor, ac fermentum lectus tristique ac. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce laoreet erat ex, in sodales magna sodales eu.</p>\r\n\r\n<p>Nullam posuere placerat ultrices. Aenean risus magna, pharetra eu hendrerit pretium, volutpat id mauris. Morbi efficitur leo non enim sodales efficitur. Praesent eu luctus lectus. Mauris sed mauris erat. Vivamus aliquet ornare ipsum quis tempus. In sed semper libero, sed ornare purus. Donec leo augue, venenatis et gravida sit amet, accumsan a turpis. Vivamus ullamcorper ante at justo gravida bibendum. Vivamus mattis dapibus dui. Praesent vel pellentesque nisl. Cras lorem erat, maximus sit amet sollicitudin eget, mattis eget libero. Vivamus sed elementum mi. Donec posuere neque sapien, quis rhoncus risus fringilla at. Nulla interdum laoreet dui id convallis. Pellentesque ac faucibus quam.</p>\r\n','2016-09-16 14:13:20','2016-09-16 14:13:20'),(2,2,'2016-09-15','Vivamus fringilla condimentum diam','vivamus-fringilla-condimentum-diam','Fusce sed congue magna. Donec tortor arcu, tincidunt eget maximus vitae, dapibus ut nibh.','22 min','pexels-photo-128639_20160916141402.jpeg','<p>Mauris vitae risus et elit sagittis aliquam nec non libero. Vivamus fringilla condimentum diam, vel mollis orci tristique sed. Nulla nec urna et eros egestas luctus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.&nbsp;Phasellus pellentesque cursus turpis, eu volutpat justo egestas a. Fusce sed congue magna. Donec tortor arcu, tincidunt eget maximus vitae, dapibus ut nibh. Ut sit amet velit in quam placerat placerat vitae ac mi. Phasellus pulvinar enim a purus ornare, quis pharetra augue lobortis. Sed non tristique tellus. Duis mollis, velit vel mollis tristique, erat tellus sollicitudin libero, non mollis dolor nisi eget dolor. Aliquam erat volutpat. Nunc hendrerit turpis ante, id elementum nibh finibus ac.</p>\r\n\r\n<p>Nam vel sollicitudin diam. Suspendisse potenti. Nulla dignissim magna ultrices condimentum tempus. Integer rhoncus vel ligula in laoreet. Proin eleifend eros in velit viverra sollicitudin. Quisque pulvinar molestie semper. Cras semper posuere interdum. Quisque dignissim feugiat dolor, ac fermentum lectus tristique ac. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce laoreet erat ex, in sodales magna sodales eu.</p>\r\n\r\n<p>Nullam posuere placerat ultrices. Aenean risus magna, pharetra eu hendrerit pretium, volutpat id mauris. Morbi efficitur leo non enim sodales efficitur. Praesent eu luctus lectus. Mauris sed mauris erat. Vivamus aliquet ornare ipsum quis tempus. In sed semper libero, sed ornare purus. Donec leo augue, venenatis et gravida sit amet, accumsan a turpis. Vivamus ullamcorper ante at justo gravida bibendum. Vivamus mattis dapibus dui. Praesent vel pellentesque nisl. Cras lorem erat, maximus sit amet sollicitudin eget, mattis eget libero. Vivamus sed elementum mi. Donec posuere neque sapien, quis rhoncus risus fringilla at. Nulla interdum laoreet dui id convallis. Pellentesque ac faucibus quam.</p>\r\n','2016-09-16 14:14:03','2016-09-16 14:14:03');
/*!40000 ALTER TABLE `conteudo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conteudo_categorias`
--

DROP TABLE IF EXISTS `conteudo_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conteudo_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conteudo_categorias`
--

LOCK TABLES `conteudo_categorias` WRITE;
/*!40000 ALTER TABLE `conteudo_categorias` DISABLE KEYS */;
INSERT INTO `conteudo_categorias` VALUES (1,1,'Categoria','categoria','2016-09-16 14:12:01','2016-09-16 14:12:01'),(2,2,'Exemplo','exemplo','2016-09-16 14:12:08','2016-09-16 14:12:08');
/*!40000 ALTER TABLE `conteudo_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_09_08_193213_create_quem_faz_table',1),('2016_09_08_194021_create_trabalho_table',1),('2016_09_08_194446_create_banners_table',1),('2016_09_09_172959_create_servicos_table',1),('2016_09_09_183057_create_conteudo_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quem_faz`
--

DROP TABLE IF EXISTS `quem_faz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quem_faz` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quem_faz`
--

LOCK TABLES `quem_faz` WRITE;
/*!40000 ALTER TABLE `quem_faz` DISABLE KEYS */;
INSERT INTO `quem_faz` VALUES (1,0,'Jorge Forbes','Integer ullamcorper ante justo, vitae luctus mi luctus eget. Vivamus eu ante augue. Cras porta lacinia velit imperdiet volutpat. Aenean venenatis pretium dolor, nec elementum libero gravida scelerisque. Integer faucibus consectetur erat, vitae porttitor turpis ornare sed. Etiam accumsan lobortis efficitur. Pellentesque luctus justo in pellentesque consequat. Integer non lectus in felis blandit vestibulum. Aliquam pharetra laoreet diam, interdum interdum mauris porta porttitor. Etiam at elit purus. Donec porttitor metus non tellus suscipit dictum. Curabitur nec mi sagittis, dictum ante ut, tempus arcu. Nulla facilisi. Nunc interdum erat mauris, sed auctor turpis rutrum semper. Vestibulum ligula purus, tristique a erat ut, bibendum consectetur quam. Integer vel mauris hendrerit neque tempus ullamcorper quis vel sapien.\r\n\r\nSed nec lacus semper, fermentum risus sed, malesuada metus. Mauris massa tortor, euismod vel lobortis sit amet, varius non odio. Aenean semper ut tortor at posuere. Mauris semper arcu leo, ac consequat purus dictum et. Integer dapibus, libero ac volutpat rhoncus, eros ipsum viverra nisi, nec tempor enim augue ac nisl. Donec at quam ante. Nullam maximus mi diam, sed malesuada massa porta ut. Vivamus eget massa sed turpis viverra posuere non a risus. Nulla efficitur, orci ut gravida aliquam, sapien ipsum consectetur nisl, vel posuere orci arcu sit amet ex. Cras eleifend lobortis malesuada.\r\n\r\nPhasellus vehicula felis id venenatis sagittis. Donec aliquet elit metus, et fermentum quam elementum mollis. Nulla dignissim nisl vel mauris egestas, in ultricies orci maximus. Donec condimentum dolor vitae interdum cursus. Vivamus eget aliquet nibh. Nulla facilisi. Aliquam id lobortis odio. Vivamus euismod sem sit amet placerat lobortis. Duis iaculis, diam sit amet pharetra gravida, ex turpis aliquet velit, nec finibus sapien velit non urna. Donec sit amet tellus condimentum, dapibus dolor venenatis, laoreet turpis. Proin faucibus dignissim risus ut vestibulum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent a mollis justo, quis dignissim ipsum.','img-perfil_20160916135657.jpg',NULL,'2016-09-16 13:56:57'),(2,1,'Silvio Genesini','Integer ullamcorper ante justo, vitae luctus mi luctus eget. Vivamus eu ante augue. Cras porta lacinia velit imperdiet volutpat. Aenean venenatis pretium dolor, nec elementum libero gravida scelerisque. Integer faucibus consectetur erat, vitae porttitor turpis ornare sed. Etiam accumsan lobortis efficitur. Pellentesque luctus justo in pellentesque consequat. Integer non lectus in felis blandit vestibulum. Aliquam pharetra laoreet diam, interdum interdum mauris porta porttitor. Etiam at elit purus. Donec porttitor metus non tellus suscipit dictum. Curabitur nec mi sagittis, dictum ante ut, tempus arcu. Nulla facilisi. Nunc interdum erat mauris, sed auctor turpis rutrum semper. Vestibulum ligula purus, tristique a erat ut, bibendum consectetur quam. Integer vel mauris hendrerit neque tempus ullamcorper quis vel sapien.','img-perfil_20160916135711.jpg',NULL,'2016-09-16 13:57:12'),(3,2,'Augusto Rodrigues','Integer ullamcorper ante justo, vitae luctus mi luctus eget. Vivamus eu ante augue. Cras porta lacinia velit imperdiet volutpat. Aenean venenatis pretium dolor, nec elementum libero gravida scelerisque. Integer faucibus consectetur erat, vitae porttitor turpis ornare sed. Etiam accumsan lobortis efficitur. Pellentesque luctus justo in pellentesque consequat. Integer non lectus in felis blandit vestibulum. Aliquam pharetra laoreet diam, interdum interdum mauris porta porttitor. Etiam at elit purus. Donec porttitor metus non tellus suscipit dictum. Curabitur nec mi sagittis, dictum ante ut, tempus arcu. Nulla facilisi. Nunc interdum erat mauris, sed auctor turpis rutrum semper. Vestibulum ligula purus, tristique a erat ut, bibendum consectetur quam. Integer vel mauris hendrerit neque tempus ullamcorper quis vel sapien.','img-perfil_20160916135718.jpg',NULL,'2016-09-16 13:57:18'),(4,3,'Ricardo Tucci','Integer ullamcorper ante justo, vitae luctus mi luctus eget. Vivamus eu ante augue. Cras porta lacinia velit imperdiet volutpat. Aenean venenatis pretium dolor, nec elementum libero gravida scelerisque. Integer faucibus consectetur erat, vitae porttitor turpis ornare sed. Etiam accumsan lobortis efficitur. Pellentesque luctus justo in pellentesque consequat. Integer non lectus in felis blandit vestibulum. Aliquam pharetra laoreet diam, interdum interdum mauris porta porttitor. Etiam at elit purus. Donec porttitor metus non tellus suscipit dictum. Curabitur nec mi sagittis, dictum ante ut, tempus arcu. Nulla facilisi. Nunc interdum erat mauris, sed auctor turpis rutrum semper. Vestibulum ligula purus, tristique a erat ut, bibendum consectetur quam. Integer vel mauris hendrerit neque tempus ullamcorper quis vel sapien.','img-perfil_20160916135723.jpg',NULL,'2016-09-16 13:57:24');
/*!40000 ALTER TABLE `quem_faz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `apresentacao` text COLLATE utf8_unicode_ci NOT NULL,
  `liderando_em_terradois` text COLLATE utf8_unicode_ci NOT NULL,
  `transformacao_cultural` text COLLATE utf8_unicode_ci NOT NULL,
  `etica_responsabilidade_crise` text COLLATE utf8_unicode_ci NOT NULL,
  `producao_e_edicao_de_cultura` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ex ex, vehicula et mi ac, consectetur dapibus ex. Nullam laoreet felis ac orci tincidunt, quis aliquam lectus pretium. Maecenas vel neque ut mauris gravida molestie sed eu nunc. Suspendisse congue ipsum id sapien bibendum, placerat maximus orci cursus. In luctus diam eget lacus commodo feugiat. Donec dictum mauris a quam rhoncus, sit amet tempus nunc cursus. Nulla eget dolor rutrum, pulvinar ipsum ac, venenatis nisl. Aenean porta nunc in imperdiet scelerisque. Aenean eget nisi feugiat ipsum lacinia sodales.</p>\r\n\r\n<p>Aenean sagittis nunc ut condimentum laoreet. Proin nec felis vitae felis feugiat faucibus. In velit quam, ultrices eu sem a, iaculis dictum metus. Pellentesque turpis justo, viverra ac fringilla at, fringilla quis eros. Cras non hendrerit augue. Donec bibendum sit amet diam quis pulvinar. Nulla placerat odio et turpis luctus, non tincidunt felis porta. Donec lorem nunc, blandit vitae imperdiet sed, dictum aliquet est. Aliquam sapien turpis, malesuada non scelerisque ac, fermentum quis ex.</p>\r\n\r\n<p>Integer ullamcorper ante justo, vitae luctus mi luctus eget. Vivamus eu ante augue. Cras porta lacinia velit imperdiet volutpat. Aenean venenatis pretium dolor, nec elementum libero gravida scelerisque. Integer faucibus consectetur erat, vitae porttitor turpis ornare sed. Etiam accumsan lobortis efficitur. Pellentesque luctus justo in pellentesque consequat. Integer non lectus in felis blandit vestibulum. Aliquam pharetra laoreet diam, interdum interdum mauris porta porttitor. Etiam at elit purus. Donec porttitor metus non tellus suscipit dictum. Curabitur nec mi sagittis, dictum ante ut, tempus arcu. Nulla facilisi. Nunc interdum erat mauris, sed auctor turpis rutrum semper. Vestibulum ligula purus, tristique a erat ut, bibendum consectetur quam. Integer vel mauris hendrerit neque tempus ullamcorper quis vel sapien.</p>\r\n','<p>Sed nec lacus semper, fermentum risus sed, malesuada metus. Mauris massa tortor, euismod vel lobortis sit amet, varius non odio. Aenean semper ut tortor at posuere. Mauris semper arcu leo, ac consequat purus dictum et. Integer dapibus, libero ac volutpat rhoncus, eros ipsum viverra nisi, nec tempor enim augue ac nisl. Donec at quam ante. Nullam maximus mi diam, sed malesuada massa porta ut. Vivamus eget massa sed turpis viverra posuere non a risus. Nulla efficitur, orci ut gravida aliquam, sapien ipsum consectetur nisl, vel posuere orci arcu sit amet ex. Cras eleifend lobortis malesuada.</p>\r\n\r\n<p>Phasellus vehicula felis id venenatis sagittis. Donec aliquet elit metus, et fermentum quam elementum mollis. Nulla dignissim nisl vel mauris egestas, in ultricies orci maximus. Donec condimentum dolor vitae interdum cursus. Vivamus eget aliquet nibh. Nulla facilisi. Aliquam id lobortis odio.</p>\r\n','<p>Sed nec lacus semper, fermentum risus sed, malesuada metus. Mauris massa tortor, euismod vel lobortis sit amet, varius non odio. Aenean semper ut tortor at posuere. Mauris semper arcu leo, ac consequat purus dictum et. Integer dapibus, libero ac volutpat rhoncus, eros ipsum viverra nisi, nec tempor enim augue ac nisl. Donec at quam ante. Nullam maximus mi diam, sed malesuada massa porta ut. Vivamus eget massa sed turpis viverra posuere non a risus. Nulla efficitur, orci ut gravida aliquam, sapien ipsum consectetur nisl, vel posuere orci arcu sit amet ex. Cras eleifend lobortis malesuada.</p>\r\n','<p>Sed nec lacus semper, fermentum risus sed, malesuada metus. Mauris massa tortor, euismod vel lobortis sit amet, varius non odio. Aenean semper ut tortor at posuere. Mauris semper arcu leo, ac consequat purus dictum et. Integer dapibus, libero ac volutpat rhoncus, eros ipsum viverra nisi, nec tempor enim augue ac nisl. Donec at quam ante. Nullam maximus mi diam, sed malesuada massa porta ut. Vivamus eget massa sed turpis viverra posuere non a risus. Nulla efficitur, orci ut gravida aliquam, sapien ipsum consectetur nisl, vel posuere orci arcu sit amet ex. Cras eleifend lobortis malesuada.</p>\r\n','<p>Sed nec lacus semper, fermentum risus sed, malesuada metus. Mauris massa tortor, euismod vel lobortis sit amet, varius non odio. Aenean semper ut tortor at posuere. Mauris semper arcu leo, ac consequat purus dictum et. Integer dapibus, libero ac volutpat rhoncus, eros ipsum viverra nisi, nec tempor enim augue ac nisl. Donec at quam ante. Nullam maximus mi diam, sed malesuada massa porta ut. Vivamus eget massa sed turpis viverra posuere non a risus. Nulla efficitur, orci ut gravida aliquam, sapien ipsum consectetur nisl, vel posuere orci arcu sit amet ex. Cras eleifend lobortis malesuada.</p>\r\n','pexels-photo-24578_20160916132722.jpg','pexels-photo-128639_20160916132722.jpeg','pattern-abstract-honeycomb-metal_20160916132722.jpg','pexels-photo-38132_20160916132723.jpeg',NULL,'2016-09-16 13:27:47');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trabalho`
--

DROP TABLE IF EXISTS `trabalho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trabalho` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trabalho`
--

LOCK TABLES `trabalho` WRITE;
/*!40000 ALTER TABLE `trabalho` DISABLE KEYS */;
INSERT INTO `trabalho` VALUES (1,'img-trabalho_20160916135614.jpg','Tempo da Descoberta','<p>Phasellus vehicula felis id venenatis sagittis. Donec aliquet elit metus, et fermentum quam elementum mollis. Nulla dignissim nisl vel mauris egestas, in ultricies orci maximus. Donec condimentum dolor vitae interdum cursus. Vivamus eget aliquet nibh. Nulla facilisi. Aliquam id lobortis odio.</p>\r\n','Tempo da Proposição','<p>Integer ullamcorper ante justo, vitae luctus mi luctus eget. Vivamus eu ante augue. Cras porta lacinia velit imperdiet volutpat. Aenean venenatis pretium dolor, nec elementum libero gravida scelerisque. Integer faucibus consectetur erat, vitae porttitor turpis ornare sed. Etiam accumsan lobortis efficitur. Pellentesque luctus justo in pellentesque consequat. Integer non lectus in felis blandit vestibulum. Aliquam pharetra laoreet diam, interdum interdum mauris porta porttitor. Etiam at elit purus. Donec porttitor metus non tellus suscipit dictum. Curabitur nec mi sagittis, dictum ante ut, tempus arcu. Nulla facilisi. Nunc interdum erat mauris, sed auctor turpis rutrum semper. Vestibulum ligula purus, tristique a erat ut, bibendum consectetur quam. Integer vel mauris hendrerit neque tempus ullamcorper quis vel sapien.</p>\r\n\r\n<p>Sed nec lacus semper, fermentum risus sed, malesuada metus. Mauris massa tortor, euismod vel lobortis sit amet, varius non odio. Aenean semper ut tortor at posuere. Mauris semper arcu leo, ac consequat purus dictum et. Integer dapibus, libero ac volutpat rhoncus, eros ipsum viverra nisi, nec tempor enim augue ac nisl. Donec at quam ante. Nullam maximus mi diam, sed malesuada massa porta ut. Vivamus eget massa sed turpis viverra posuere non a risus. Nulla efficitur, orci ut gravida aliquam, sapien ipsum consectetur nisl, vel posuere orci arcu sit amet ex. Cras eleifend lobortis malesuada.</p>\r\n\r\n<p>Phasellus vehicula felis id venenatis sagittis. Donec aliquet elit metus, et fermentum quam elementum mollis. Nulla dignissim nisl vel mauris egestas, in ultricies orci maximus. Donec condimentum dolor vitae interdum cursus. Vivamus eget aliquet nibh. Nulla facilisi. Aliquam id lobortis odio. Vivamus euismod sem sit amet placerat lobortis. Duis iaculis, diam sit amet pharetra gravida, ex turpis aliquet velit, nec finibus sapien velit non urna. Donec sit amet tellus condimentum, dapibus dolor venenatis, laoreet turpis. Proin faucibus dignissim risus ut vestibulum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent a mollis justo, quis dignissim ipsum.</p>\r\n\r\n<p>Integer ullamcorper ante justo, vitae luctus mi luctus eget. Vivamus eu ante augue. Cras porta lacinia velit imperdiet volutpat. Aenean venenatis pretium dolor, nec elementum libero gravida scelerisque. Integer faucibus consectetur erat, vitae porttitor turpis ornare sed.</p>\r\n\r\n<p>Phasellus vehicula felis id venenatis sagittis. Donec aliquet elit metus, et fermentum quam elementum mollis. Nulla dignissim nisl vel mauris egestas, in ultricies orci maximus. Donec condimentum dolor vitae interdum cursus. Vivamus eget aliquet nibh. Nulla facilisi. Aliquam id lobortis odio.</p>\r\n\r\n<p>Phasellus vehicula felis id venenatis sagittis. Donec aliquet elit metus, et fermentum quam elementum mollis. Nulla dignissim nisl vel mauris egestas, in ultricies orci maximus. Donec condimentum dolor vitae interdum cursus. Vivamus eget aliquet nibh. Nulla facilisi. Aliquam id lobortis odio.</p>\r\n','Tempo da Ação','<p>Phasellus vehicula felis id venenatis sagittis. Donec aliquet elit metus, et fermentum quam elementum mollis. Nulla dignissim nisl vel mauris egestas, in ultricies orci maximus. Donec condimentum dolor vitae interdum cursus. Vivamus eget aliquet nibh. Nulla facilisi. Aliquam id lobortis odio.</p>\r\n',NULL,'2016-09-16 13:56:14');
/*!40000 ALTER TABLE `trabalho` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$G9DClsvUHJYtz2xhVU/q4eBxEq.h3OOfrRttunNkdtGykyf8eqvN6',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-16 14:15:31
